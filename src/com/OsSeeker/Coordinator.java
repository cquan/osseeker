package com.OsSeeker;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.*;
import java.net.URL;
import java.util.Properties;

/*
 * This class should act as a starting point of all actions
 * Right now, a good process of the whole process could be: 
 * 1. ensure everything ready including headNode, pxeNode
 * 2. get the node lists with the help of ipParser from the file obtained from PxeNode
 * 3. show the current status of all nodes
 * This is the expectation for version 1
 */
public class Coordinator {
    public static final String OS_FILE_NAME = "install.log";

    public static void main(String[] args) throws SftpException {
        System.out.println("Hello");

        JSch jsch = new JSch(); //JSch here is acting as a central configuration point here for ssh sessions
        System.out.println("Test Node Class");
        //HeadNode head = new HeadNode(host, user, password, jsch);
        HeadNode head = new HeadNode(jsch);

        PxeNode pxe = new PxeNode(jsch);

        //check if the user want to reset all preferences
        if ( args.length > 0 && args[0].equals("reset") ) {
            head.resetPrefs();
            pxe.resetPrefs();
        }
        head.initNode();
        pxe.initNode();

        System.out.println("HeadNode reachable: " + head.isReachable());
        System.out.println("PxeNode reachable: " + pxe.isReachable());

        if (pxe.isReachable()) {
            try {
                pxe.getInstallLog();
            } catch (JSchException e) {
                e.printStackTrace();
            }
        } else if (head.isReachable()) {
            try {
                System.out.println("Borrow session from head");
                pxe.getInstallLog(head.borrowSession());
            } catch (JSchException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Connection failed, please check network");
        }
        File f = new File(PxeNode.INSTALL_LOCAL_PATH);
        if ( f.exists()) {
            SimplePresenter.printOsInfo(f);
        }else {
            System.out.println("Coordinator: install.log not found");
        }

    }


    //this method is designed to check if file existed in classpath
    public static boolean fileExists(String fileName){
        URL filePath = Coordinator.class.getResource(fileName);
        if(filePath != null) {
            System.out.println(fileName + " found");
            return true;
        }
        System.out.println(fileName + " not found");
        return false;
    }


}

