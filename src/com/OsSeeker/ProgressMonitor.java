package com.OsSeeker;

import com.jcraft.jsch.SftpProgressMonitor;

/**
 * Created by chenhui on 13/04/2015.
 */
public class ProgressMonitor implements SftpProgressMonitor {
    long fileSize; //holds the size of the file to be transferred
    long transferedSize = 0;
    @Override
    public void init(int i, String s, String s1, long l) {
        System.out.println("File transfer starts");
        this.fileSize = l;
    }

    @Override
    public boolean count(long bytes) {
        transferedSize += bytes;
        int percent = (int)( (double)transferedSize / (double)fileSize * 100 );
        this.printProgBar(percent);
        //System.out.print("#");
        return (true);
    }

    @Override
    public void end() {
        System.out.println("File transfer ends");
    }

    //this method gives a fancier progress bar
    private void printProgBar(int percent){
        StringBuilder bar = new StringBuilder("[");

        for(int i = 0; i < 50; i++){
            if( i < (percent/2)){
                bar.append("=");
            }else if( i == (percent/2)){
                bar.append(">");
            }else{
                bar.append(" ");
            }
        }

        bar.append("]   " + percent + "%     ");
        System.out.print("\r" + bar.toString());
    }

    
}
