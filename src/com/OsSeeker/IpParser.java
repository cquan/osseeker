package com.OsSeeker;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by iota on 04/04/2015.
 */
public class IpParser {

    //hashmap: IP -> timestamp -> LogEntry
    //note one IP may have more than one pair of 'start'/'stop'
    private static HashMap<String,ArrayList<LogEntry>> hostnameToEntries = new HashMap();
    private static ArrayList<ParsedNode> parsedNodes = new ArrayList();
    private static ArrayList<ParsedNode> pendingParsedNodes = new ArrayList();
    private static ArrayList<ParsedNode> stoppedParsedNodes = new ArrayList();
    private static ArrayList<ParsedNode> headLessParsedNodes = new ArrayList();
    /**
     *
     */
    public static ArrayList<ParsedNode> parse(Path filePath, Timestamp fromwhen){
        parseInternal(filePath, fromwhen);
        mergeLogEntriesToParsedNode();
        parsedNodes.sort(new ParsedNode.TimeGeneralComparator());
        System.out.println("parsedNodes.size() is " + parsedNodes.size());
        return parsedNodes;
        /*
        for (String hostName : parsedNodes.keySet()){
            ParsedNode pnode = (ParsedNode) parsedNodes.get(hostName);
            System.out.println(pnode.toString());
        }
        */
    }

    /**
     * extract the most recent pair of start/stop log for each IP address in hostnameToEntries
     */
    private static void mergeLogEntriesToParsedNode(){
        for (String hostName : hostnameToEntries.keySet()){
            ArrayList<LogEntry> entries = (ArrayList<LogEntry>) hostnameToEntries.get(hostName);

            /*
            System.out.println(hostName);
            System.out.println("pre-sort:");
            for(Timestamp t: entries.keySet().toArray(new Timestamp[0])){
                System.out.println("\t"+t.toString()+"\t"+entries.get(t));
            }
            */
            entries.sort(new LogEntry.TimeComparator());


            /*
            System.out.println("post-sort:");
            for(Timestamp t: ascending_entries){
                System.out.println("\t"+t.toString()+"\t"+entries.get(t));
            }
            */
            String host = hostName.toString();
            LogEntry lastLogEntry = entries.get(entries.size() - 1);
            String profileType = lastLogEntry.getProfileType();
            String osProfile = lastLogEntry.getOsProfile();

            //system installation pending or exited abnormally
            if(lastLogEntry.isStart()){
                Timestamp timeStart = lastLogEntry.getTimeStamp();
                //set stop time to 1970 Jan 00:00 UTC+0
                Timestamp timeStop = new Timestamp(0);
                ParsedNode parsedNode = ParsedNode.getInstance(host, profileType, osProfile, timeStart, timeStop);
                //System.out.println("########### "+lastLogEntry);
                parsedNodes.add(parsedNode);
                pendingParsedNodes.add(parsedNode);
            }

            if(entries.size()>=2){
                LogEntry secondLastLogEntry = entries.get(entries.size()-2);
                if(lastLogEntry.isStop() && secondLastLogEntry.isStart()
                && lastLogEntry.isOsNameIdenticalTo(secondLastLogEntry)
                && lastLogEntry.isProfileIdenticalTo(secondLastLogEntry))
                {
                    Timestamp timeStart = secondLastLogEntry.getTimeStamp();
                    Timestamp timeStop = lastLogEntry.getTimeStamp();
                    ParsedNode parsedNode = ParsedNode.getInstance(host, profileType, osProfile, timeStart, timeStop);
                    parsedNodes.add(parsedNode);
                    stoppedParsedNodes.add(parsedNode);
                }
            }
        }

    }

    private static Timestamp[] sortByTimeStamp(Timestamp[] entries){
        Utility.QuickSort.sortDefaultAscending(entries);
        return entries;
    }
    /**
     *
     */
    private static void parseInternal(Path filePath, Timestamp fromWhen){
        try (BufferedReader reader = Utility.getInstallLogReader(filePath, fromWhen)) {
            String line;
            while ((line = reader.readLine()) != null) {
                LogEntry entry = parseLine(line);
                String hostNameHere = entry.getHostName().toString();
                if(hostnameToEntries.keySet().contains(hostNameHere)){
                    hostnameToEntries.get(hostNameHere).add(entry);
                }
                else{
                    ArrayList<LogEntry> entries = new ArrayList<LogEntry>();
                    entries.add(entry);
                    hostnameToEntries.put(hostNameHere, entries);
                }

            }
            System.out.println("hostnameToEntries.keySet().size() is "+hostnameToEntries.keySet().size());
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    /**
     *
     * @param line
     * @return
     */
    private static LogEntry parseLine(String line){
        LogEntry out = null;
        String myregex = "";
        myregex+="^";//line beginning
        myregex+="[\\s]*([^\\s]+)";//profile type
        myregex+="[\\s]*([^\\s]+)";//os name
        myregex+="[\\s]*([\\d]{1,3}[.][\\d]{1,3}[.][\\d]{1,3}[.][\\d]{1,3})";//hostName
        myregex+="[\\s]*(\\Qstop\\E|\\Qstart\\E)";//stop or start
        myregex+="[\\s]*([\\d.]+)";//timestamp
        myregex+="$";

        Pattern pattern = Pattern.compile(myregex);
        Matcher matcher = pattern.matcher(line);
        if(matcher.find()) {
            String profileType  = matcher.group(1);
            String osName       = matcher.group(2);
            String ip           = matcher.group(3);
            String status       = matcher.group(4);
            String timeStamp    = matcher.group(5);
            out = LogEntry.getInstance(profileType, osName, ip, status, timeStamp);
        }
        return out;
    }
}
