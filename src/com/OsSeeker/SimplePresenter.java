package com.OsSeeker;

import org.apache.commons.io.*;
import org.apache.commons.io.input.ReversedLinesFileReader;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chenhui on 17/04/2015.
 * This class is used to simply present the content of the install.log file
 * It is used to model the behavior of IpParser in the early stage of the project
 *      use ReversedLinesFileReader from apache for reverse read
 */
public class SimplePresenter {

    static int lineNum = 10; //default lines to be printed


    //reversely print the install.log file
    public static void printOsInfo(File file){
        try {
            ReversedLinesFileReader rlf;
            rlf = new ReversedLinesFileReader( file );
            for (int i = 0; i < lineNum; i++) {
                SimplePresenter.parseLine(rlf.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //this is the simple method to parse one line of the install.log
    //from cobbler's source code, one line from install.log consists of:
    //    fd.write("%s\t%s\t%s\tstart\t%s\n" % (objtype, name, ip, time.time()))
    //    fd.write("%s\t%s\t%s\tstop\t%s\n" % (objtype, name, ip, time.time()))
    //so we can split the string with "\t"
    //example of time stamp: 1406715437.16
    //System current mills:  1429385086610
    private static void parseLine(String s){
        String[] parts = s.split("\t");

        //here the time stamps string we got are generated from python so to 
        //  convert them to java millis, we need to multiply them by 1000
        long lTimestamp = (long)(Double.parseDouble(parts[4]) * 1000);
        Timestamp timestamp = new Timestamp( lTimestamp );
        Date date = new Date(timestamp.getTime());
        System.out.println(parts[2] + "\t" + parts[3] + "\t" + date );
    }
}
