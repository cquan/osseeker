package com.OsSeeker;

import com.jcraft.jsch.*;

import java.io.File;

/**
 * Created by chenhui on 04/04/2015.
 * The PxeNode class presents the pxe server where we can get the ip lists of the installed
 * The actions on this server should also be limited
 */
public class PxeNode extends Node {

    final static String INSTALL_LOG_PATH = "/var/log/cobbler/install.log";
    final static String INSTALL_LOCAL_PATH = "./install.log";
    final static String SAVED_DIR = ".";

    public PxeNode(JSch j) {
        super(j);
    }

    public PxeNode(String host, String user, String pswd, JSch ssher) {
        super(host, user, pswd, ssher);
    }


    // 1. check if pxeNode is reachable if so get the file; if not
    // 2. get session from headNode and do a port forward then get the new session to get the file
    public void getInstallLog() throws SftpException, JSchException {
        getFile(this.host, 22);
    }

    public void getInstallLog(Session s) throws SftpException, JSchException {
        System.out.println("The first session is connected:" + s.isConnected());

        //forward the session 
        int assignPort = s.setPortForwardingL(0, host, 22);
        System.out.println("The assigned port is " + assignPort );
        getFile("127.0.0.1", assignPort);
        s.disconnect(); 
    }

    //this method is used for getting install.log file
    //targetHost is the host to connect to, port is th port used for ssh connection
    private void getFile(String targetHost, int port) throws JSchException, SftpException {
        Session pxeSession = this.getConnectedSession(this.user, targetHost, port);

        boolean isSessionConnected = pxeSession.isConnected();
        System.out.println("System Status:" + pxeSession.getHost() + ":" + isSessionConnected);

        //start sftp channel
        ChannelSftp sftp = (ChannelSftp)pxeSession.openChannel("sftp");

        sftp.connect();
        sftp.get(INSTALL_LOG_PATH, SAVED_DIR, new ProgressMonitor());
        sftp.disconnect();
        //disconnect all sessions
        pxeSession.disconnect();
        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        //check if the file got from remote exists
        File f = new File(INSTALL_LOCAL_PATH);
        if(f.exists()) {
            System.out.println("Install.log file found");
        }
    }
}
