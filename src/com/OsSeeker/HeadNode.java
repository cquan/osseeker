package com.OsSeeker;

import com.jcraft.jsch.*;

import java.util.prefs.Preferences;

/**
 * Created by chenhui on 04/04/2015.
 * This is class coreesponds to the head node. It can be used to remotely access the cluster with web address
 * In this class, actions are strictly limited. Ideally, it only need to have one method: jumpHost to get to
 * the pxe server
 */
public class HeadNode extends Node {

    public HeadNode(String host, String user, String pswd, JSch ssher) {
        super(host, user, pswd,ssher);

    }

    public HeadNode(JSch j) {
        super(j);
    }


    /*this method is used for nodes that can only be accessed from head node.
             In this method, we only create such session and let whatever node
             choose to connect or disconnect it.*/
    public Session borrowSession() throws JSchException {
        return this.getConnectedSession(this.user,this.host,this.port);
    }

}
