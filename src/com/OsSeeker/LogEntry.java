package com.OsSeeker;

import java.sql.Timestamp;
import java.util.Comparator;

import com.OsSeeker.Utility.HostName;

/**
 * Created by iota on 09/05/2015.
 * A LogEntry is the parsed content of a line from install log
 */
public class LogEntry {
    private String profileType;
    private String osProfile;
    private HostName hostName;
    private String status;
    private Timestamp timeStamp;

    private LogEntry(String profileType,
                     String osName,
                     String ip,
                     String status,
                     String timeStamp) {
        this.profileType = profileType.trim().toLowerCase();
        this.osProfile = osName.trim().toLowerCase();
        this.hostName = Utility.HostName.getInstance(ip.trim().toLowerCase());
        this.status = status.trim().toLowerCase();
        this.timeStamp = Utility.getTimestampFromLogTimeStamp(timeStamp.trim().toLowerCase());

    }

    public static LogEntry getInstance(String profileType,
                                       String osName,
                                       String ip,
                                       String status,
                                       String timeStamp){
        return new LogEntry(profileType, osName, ip, status, timeStamp);
    }

    public boolean isStop(){
        return "stop".equals(status);
    }

    public boolean isStart(){
        return "start".equals(status);
    }

    public boolean isOsNameIdenticalTo(LogEntry counterpart){
        return this.osProfile.equals(counterpart.getOsProfile());
    }

    public boolean isProfileIdenticalTo(LogEntry counterpart){
        return this.profileType.equals(counterpart.getProfileType());
    }

    public HostName getHostName(){
        return hostName;
    }

    public String getProfileType(){
        return profileType;
    }

    public String getOsProfile(){
        return osProfile;
    }

    public Timestamp getTimeStamp(){
        return timeStamp;
    }

    public static class TimeComparator implements Comparator<LogEntry> {
        @Override
        public int compare(LogEntry l1, LogEntry l2) {
            return l1.getTimeStamp().compareTo(l2.getTimeStamp());
        }
    }

    @Override
    public String toString(){
        return profileType+" "+ osProfile +" "+ hostName +" "+status+" "+timeStamp;
    }
}
