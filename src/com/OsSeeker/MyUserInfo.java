package com.OsSeeker;

import com.jcraft.jsch.UserInfo;

import java.util.Scanner;

// This class here implements from the UserInfo class.
// It's main aim is to let program automatically sign in the server without asking for password and username

public class MyUserInfo implements UserInfo {
    String password = null;
    Scanner reader;
    

    @Override
    public String getPassphrase() {
        // TODO Auto-generated method stub
        return null;
    }


    public void setPassword(String pswd) {
        password = pswd;
    }
    @Override
    public boolean promptPassphrase(String arg0) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public boolean promptYesNo(String arg0) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public void showMessage(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean promptPassword(String arg0) {
        System.out.println("Wrong password?  Enter\n 1: change password \n Anything else: cancel");
        reader = new Scanner(System.in);
        String choice = reader.nextLine();
        if ( choice.equals("1") ) {
            return true;
        } else {
            System.out.println("Auth Cancel");
            return false;
        }
    }

    @Override
    public String getPassword() {
        reader = new Scanner(System.in);
        System.out.println("Please input password: ");
        this.password = reader.nextLine();
        return password;
    }

}
